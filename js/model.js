//Định nghĩa lớp đối tượng Sinh Viên dễ sử dụng các đối tượng cùng thuộc tính và dễ tái sử dụng + quản lí.
//Trong lớp đối tượng các phần tử phân biệt bằng dấu chấm phẩy thay vì dấu phẩy như đối tượng.
function SinhVien(ma, ten, email, matkhau, toan, li, hoa){
    this.ma = ma;
    this.ten = ten;
    this.email = email;
    this.matKhau = matkhau;
    this.toan = toan;
    this.li = li;
    this.hoa = hoa;
    this.tinhDTB = function(){
        return ((this.toan + this.li + this.hoa) / 3);
    }
}