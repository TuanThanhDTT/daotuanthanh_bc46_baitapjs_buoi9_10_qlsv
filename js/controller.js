//Chức năng lấy item trong array dssv render lên màn hình
function renderDSSV(all_item_array_dssv){
    var contentHTML = "";
    for(var i = 0; i < all_item_array_dssv.length; i++){
        var item_sv = all_item_array_dssv[i];
        var content = `<tr>
            <td>${item_sv.ma}</td>
            <td id="tenSv">${item_sv.ten}</td>
            <td id="mailSv">${item_sv.email}</td>
            <td id="updateDTB">${item_sv.tinhDTB()}</td>
            <td>
                <button onclick="xoaSinhVien('${item_sv.ma}')" class="btn btn-danger">Xóa</button>
                <button onclick="suaSinhVien('${item_sv.ma}')" class="btn btn-warning">Sửa</button>
            </td>
        </tr>`
        contentHTML = contentHTML + content;
    }
    document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

//Chức năng lấy thông tin từ form
function layThongTinTuForm(){
        //lấy giá trị user nhập từ form
        var ma = document.querySelector("#txtMaSV").value;
        var ten = document.getElementById("txtTenSV").value;
        var email = document.getElementById("txtEmail").value;
        var matKhau = document.querySelector("#txtPass").value;
        var toan = document.getElementById("txtDiemToan").value*1;
        var ly = document.getElementById("txtDiemLy").value*1;
        var hoa = document.getElementById("txtDiemHoa").value*1;
        //truyền giá trị vào lớp đối tượng SinhVien trong file model.js , có thể hiểu là gọi lại hàm Sinh Vien đã được định nghĩa ở file model.js và truyền giá trị người dùng nhập vào.
        return new SinhVien(ma,ten,email,matKhau,toan,ly,hoa);
}

//Show thông tin lên form, đi định nghĩa hàm sẽ thực thi khi gọi lại và truyền giá trị là đối tượng tại index cho tham số của function. 
function showThongTinLenForm(one_in_all_item_array_dssv){
    document.querySelector("#txtMaSV").value = one_in_all_item_array_dssv.ma;
    document.getElementById("txtTenSV").value = one_in_all_item_array_dssv.ten;
    document.getElementById("txtEmail").value = one_in_all_item_array_dssv.email;
    document.querySelector("#txtPass").value = one_in_all_item_array_dssv.matKhau;
    document.getElementById("txtDiemToan").value = one_in_all_item_array_dssv.toan;
    document.getElementById("txtDiemLy").value = one_in_all_item_array_dssv.li;
    document.getElementById("txtDiemHoa").value = one_in_all_item_array_dssv.hoa;
}

