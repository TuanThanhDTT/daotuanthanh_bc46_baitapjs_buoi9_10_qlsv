var dssv = [];

//lấy dữ liệu từ localStorage lúc user load trang
var dataJson = localStorage.getItem("DSSV");
if(dataJson != null){
    dssv = JSON.parse(dataJson).map(function(item_array_dssv){
        return new SinhVien(item_array_dssv.ma, item_array_dssv.ten, item_array_dssv.email, item_array_dssv.matKhau, item_array_dssv.toan, item_array_dssv.li, item_array_dssv.hoa);
    });
    renderDSSV(dssv);
}
function themSinhVien(){
    //Thay vì lấy giá trị từ form rồi lưu vào các biến ở file main.js, ta dùng lớp đối tượng SinhVien() thực hiện tách code ra 2 file model.js và controler.js để dễ gọn code file main.js, góp phần quản lí code, xem lại sau này.
    var sv = layThongTinTuForm();

    //validate
    //Kiểm tra mã sinh viên
    //Ap dụng && để kiểm tra true false 1 trong 2 tiêu chí, "khác mã và quy định độ dài mã" nếu tiêu chí nào false thì showMessage tại tiêu chí đó.
    var isValid = kiemTraTrung(sv.ma,dssv) && kiemTraDoDai(5, 5, "spanMaSV", sv.ma, "Mã sinh viên không hợp lệ!");
    
    //Kiểm tra mật khẩu
    isValid = isValid & kiemTraDoDai(4, 8, "spanMatKhau", sv.matKhau, "Mật khẩu không hợp lệ!");

    //Kiểm tra email
    isValid &= kỉemTraEmail(sv.email);
    
    //Biến isValid ở đây khi gọi các hàm kiểm tra sẽ trả về giá trị true false, nếu tất cả true thì push, ngược lại false ở đâu sửa tại đó nhờ hàm showMessage thông báo!
    if(isValid){
        //push đối tượng sv vào mảng dssv
        dssv.push(sv);
        renderDSSV(dssv);
    
        //Lưu danh sách sinh viên xuống localStorage dưới dạng jSon
        var dataJson = JSON.stringify(dssv);
        localStorage.setItem("DSSV", dataJson);

         //reset => dùng thẻ form để reset thẻ input
         document.getElementById("formQLSV").reset();
    }
}

function xoaSinhVien(ma_id_sinhvien){
    console.log(ma_id_sinhvien);
    //Sử dụng findIndex để lấy index và Splice để xóa
    var index = dssv.findIndex(function(one_to_all_item_array_dssv){
        return one_to_all_item_array_dssv.ma == ma_id_sinhvien;
    });
    console.log(index);
    //Phương thức splice cần truyền vào "index" và "số phần tử"  mỗi lần splice
    dssv.splice(index, 1);
    renderDSSV(dssv);
    var dataJson = JSON.stringify(dssv);
    localStorage.setItem("DSSV", dataJson);
}

function suaSinhVien(ma_id_sinhvien){
    //Sử dụng findIndex để lấy index, findIndex sẽ duyệt từng phần tử trong mảng dssv và so mã id sinh viên đến khi true sẽ trả về index cho biến lưu lại.
    var index = dssv.findIndex(function(one_to_all_item_array_dssv){
    return one_to_all_item_array_dssv.ma == ma_id_sinhvien;
    });
    showThongTinLenForm(dssv[index]);
}

function capNhatSinhVien(){

    //Sau khi click nút sửa, thông tin sinh viên tại index nút sửa  show lại lên form, gọi lại hàm lấy thông tin lưu vào biến sv
    
    var sv = layThongTinTuForm();
    console.log(sv.ma);

    //Duyệt mảng dssv để tìm phần tử dssv[i] có mã trùng với đối tượng sv.ma mình vừa lấy ở trên

    for(var i = 0; i < dssv.length; i++){
        if(dssv[i].ma == sv.ma){
            dssv[i].ten = sv.ten;
            dssv[i].email = sv.email;
            dssv[i].toan = sv.toan;
            dssv[i].li = sv.li;
            dssv[i].hoa = sv.hoa;
            //Dom đến id đã đặt ở thẻ td file controller
            document.getElementById("tenSv").innerHTML = dssv[i].ten;
            document.getElementById("mailSv").innerHTML = dssv[i].email;
            document.getElementById("updateDTB").innerHTML = dssv[i].tinhDTB();
        }    
    }
    console.log(dssv);

    //Phải render lại để thông tin được cập nhật đúng vị trí
    renderDSSV(dssv);
    //Lưu danh sách sinh viên xuống localStorage dưới dạng jSon
    var dataJson = JSON.stringify(dssv);
    localStorage.setItem("DSSV", dataJson);
    }
    

