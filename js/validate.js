function showMessage(idSpan, message){
    document.getElementById(idSpan).innerText = message;
}

function kiemTraTrung(ma_id_sinhvien, dssv){
    var index = dssv.findIndex(function(one_to_all_item_array_dssv){
        return one_to_all_item_array_dssv.ma == ma_id_sinhvien;
    });
    console.log(index);
    //Nếu tìm được index thì chứng tỏ có mã trùng trong mảng => false => gọi hàm showMessage thông báo mã đã tồn tại 
    if (index == -1){
        showMessage("spanMaSV","");
        return true;
    } else {
        showMessage("spanMaSV","Mã sinh viên đã tồn tại!");
        return false;
    }
}

function kiemTraDoDai(min, max, idSpan, value, message){
    var length = value.length;
    if(length >= min && length <= max){
        showMessage(idSpan, "");
        return true;
    }else{
        showMessage(idSpan, message);
        return false;
    }
}

function kỉemTraEmail(email){
    const re =
  /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  var isEmail = re.test(email) // phương thức test trả giá trị true false, nếu email truyền vào thỏa re thì trả về true
  if(isEmail){
    showMessage("spanEmailSV","");
    return true;
  } else{
    showMessage("spanEmailSV","Email không hợp lệ");
    return false;
  }
}